<?php
/**
 * Main theme file.
 *
 * @package peko/theme
 */

namespace Peko\Theme;

use Peko\Theme\Elementor\Send_Grid_Form_Action;

/**
 * Main class file.
 */
class Main {
	/**
	 * Main construct.
	 */
	public function __construct() {
		$this->init();
	}

	/**
	 * Init hooks and actions.
	 *
	 * @return void
	 */
	public function init(): void {
		add_action( 'elementor_pro/forms/actions/register', [ $this, 'register_send_grid_form_actions' ] );
	}

	/**
	 * Register new action add form send grid.
	 *
	 * @param $form_actions_registrar
	 *
	 * @return void
	 */
	public function register_send_grid_form_actions( $form_actions_registrar ) {
		$form_actions_registrar->register( new Send_Grid_Form_Action() );
	}
}
