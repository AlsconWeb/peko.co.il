<?php
/**
 * SendGrid api.
 *
 * @package peko/theme
 */

namespace Peko\Theme\Api;

use SendGrid as SendGridApi;

/**
 * SendGrid class file.
 */
class SendGrid {

	/**
	 * Api key SendGrid.
	 *
	 * @var string
	 */
	private string $api_key;

	/**
	 * SendGrid api liberty.
	 *
	 * @var SendGrid
	 */
	private $send_grid_lib;

	/**
	 * SendGrid construct.
	 */
	public function __construct( string $api_key ) {
		$this->api_key       = $api_key;
		$this->send_grid_lib = new SendGridApi( $this->api_key );
	}

	/**
	 * Add new contact in send grid
	 *
	 * @param $form_data
	 *
	 * @return bool
	 */
	public function add_recipients( $form_data ) {
		try {

			$response = $this->send_grid_lib->client->marketing()->contacts()->put( $form_data );
			$body     = json_decode( $response->body() );

			if ( isset( $body->job_id ) ) {
				return true;
			}

		} catch ( Exception $ex ) {
			error_log( $ex->getMessage() );
		}

		return false;
	}
}
