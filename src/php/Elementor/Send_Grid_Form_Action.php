<?php
/**
 * Add action Send Grid.
 *
 * @package peko/theme
 */

namespace Peko\Theme\Elementor;

use Elementor\Controls_Manager;
use ElementorPro\Modules\Forms\Classes\Action_Base;
use Peko\Theme\Api\SendGrid;

/**
 * Send_Grid_Form_Action class file.
 */
class Send_Grid_Form_Action extends Action_Base {

	/**
	 * Add label.
	 *
	 * @return string
	 */
	public function get_label(): string {
		return __( 'SendGrid', 'generatepress' );
	}

	/**
	 * Action after form send.
	 *
	 * @param $record
	 */
	public function run( $record, $ajax_handler ) {
		$form_data = $record->get( 'sent_data' );
		$email     = ! empty( $form_data['email'] ) ? filter_var( wp_unslash( $form_data['email'] ), FILTER_VALIDATE_EMAIL ) : null;
		$full_name = ! empty( $form_data['firstname'] ) ? filter_var( wp_unslash( $form_data['firstname'] ), FILTER_SANITIZE_FULL_SPECIAL_CHARS ) : null;

		$from_settings = $record->get( 'form_settings' );
		$api_key       = ! empty( $from_settings['action_api_key'] ) ? filter_var( $from_settings['action_api_key'], FILTER_SANITIZE_FULL_SPECIAL_CHARS ) : null;
		$list_id       = ! empty( $from_settings['list_id'] ) ? filter_var( $from_settings['list_id'], FILTER_SANITIZE_FULL_SPECIAL_CHARS ) : null;

		$name_array = explode( ' ', $full_name );
		if ( ! empty( $api_key ) && ! empty( $list_id ) ) {
			$send_grid = new SendGrid( $api_key );

			$data = json_decode(
				'{
					    "list_ids":["' . $list_id . '"],
					    "contacts": [
					        {
					            "email": "' . $email . '",
					            "first_name": "' . $name_array[0] . '",
					            "last_name": "' . $name_array[1] . '"
					        }
					    ]
					}'
			);

			$send_grid->add_recipients( $data );
		}
	}

	/**
	 * Register control section.
	 *
	 * @param $form
	 *
	 * @inheritDoc
	 */
	public function register_settings_section( $form ) {
		$form->start_controls_section(
			'send_grid_action_section',
			[
				'label'     => esc_html__( 'SendGrid settings', 'generatepress' ),
				'condition' => [
					'submit_actions' => $this->get_name(),
				],
			]
		);

		$form->add_control(
			'action_api_key',
			[
				'label' => esc_html__( 'API Key', 'generatepress' ),
				'type'  => Controls_Manager::TEXT,
			]
		);

		$form->add_control(
			'list_id',
			[
				'label' => esc_html__( 'SendGrid list ID', 'generatepress' ),
				'type'  => Controls_Manager::TEXT,
			]
		);

		$form->end_controls_section();
	}

	/**
	 * Add action name.
	 *
	 * @return string
	 */
	public function get_name(): string {
		return 'send_grid';
	}

	/**
	 * No export.
	 *
	 * @inheritDoc
	 */
	public function on_export( $element ) {
		return $element;
	}
}
