<?php
/**
 * GeneratePress.
 *
 * Please do not make any edits to this file. All edits should be done in a child theme.
 *
 * @package GeneratePress
 */

use Peko\Theme\Main;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
function my_theme_enqueue_styles() {
	$parenthandle = 'parent-style'; // This is 'twentyfifteen-style' for the Twenty Fifteen theme.
	$theme        = wp_get_theme();
	wp_enqueue_style( $parenthandle, get_template_directory_uri() . '/style.css',
		[],  // if the parent theme code has a dependency, copy it to here
		$theme->parent()->get( 'Version' )
	);
	wp_enqueue_style( 'child-style', get_stylesheet_uri(),
		[ $parenthandle ],
		$theme->get( 'Version' ) // this only works if you have Version in the style header
	);
}

function hook_javascript() {
	?>
	<script src="https://code.jquery.com/jquery-3.6.4.min.js"
			integrity="sha256-oP6HI9z1XaZNBrJURtCoUT5SUnxFr8s3BzRl+cbzUq8=" crossorigin="anonymous"></script>
	<script type="text/javascript">
		jQuery( document ).ready( function() {

			jQuery( document ).on( 'submit_success', function() {
				//	alert('x');
				setTimeout( function() {
					jQuery( '.nfrm' ).hide();
					jQuery( '.msgblock' ).show();
					jQuery( 'html, body' ).animate( {
						scrollTop: jQuery( '.cform' ).offset().top - 100
					}, 500 );
				}, 100 );
			} );


		} );
	</script>
	<?php
}

add_action( 'wp_head', 'hook_javascript' );

add_action(
	'elementor_pro/forms/new_record',
	function ( $record, $handler ) {
		// make sure its our form.
		$form_name = $record->get_form_settings( 'form_name' );

		// Replace MY_FORM_NAME with the name you gave your form.
		if ( 'lp_waiting_list_form' !== $form_name ) {
			return;
		}

		$raw_fields = $record->get( 'fields' );
		$fields     = [];

		foreach ( $raw_fields as $id => $field ) {
			$fields[ $id ] = $field['value'];
		}

		// Replace HTTP://YOUR_WEBHOOK_URL with the actuall URL you want to post the form to.
		wp_remote_post(
			'https://members.viplus.com/subscribe.aspx?exists=merge&restore=Restore&source=API&apikey=9c1a54fa-7f01-46a4-9463-b089770c2a72  
&viplists=786735',
			[
				'body' => $fields,
			]
		);
	},
	10,
	2
);

require_once __DIR__ . '/vendor/autoload.php';

new Main();
